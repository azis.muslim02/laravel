<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form() {

        return view('data.form');
    }

    public function post(Request $request) 
    
    {
        //($request->all());
        $nama_awal= $request->nama_awal;
        $nama_akhir= $request->nama_akhir;
        $keterangan = $request->keterangan;

        return view('data.index', compact('nama_awal', 'nama_akhir', 'keterangan'));
      }
}
