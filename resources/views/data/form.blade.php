<!DOCTYPE html>
<html>
<head>
<title>Buat Account Baru</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/post" method="POST">
   @csrf
    <label for="nama">First name</label><br><br>
    <input Type="text" name="nama_awal" id="nama_awal"><br><br>
    <label for="nama">Last name</label><br><br>
    <input Type="text" name="nama_akhir" id="nama_akhir"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio" name="Gender">Male<br>
    <input type="radio" name="Gender">Female<br>
    <input type="radio" name="Gender">Other<br><br>
    <label> Nationality:</label><br><br> 
    <select>
        <option value="Indonesia" name="nationality">Indonesian</option>
        <option value="Malaysia" name="nationality">Singaporean</option>
        <option value="Lainnya" name="nationality">Malaysian</option>
        <option value="Lainnya" name="nationality">Australian</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox"> Bahasa Indonesia<br>
    <input type="checkbox"> English<br>
    <input type="checkbox"> Other<br><br>
    

    <label for="keterangan">Bio:</label><br><br>
    <textarea name="bio" id="keterangan" rows="10" cols="35"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
</body>
</html>
